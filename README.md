# Trabalho1 - Processador ICMC

Video Explicativo: https://drive.google.com/file/d/1M5L7iBYSSIof0y3v62gS3E9_5v07jxx_/view?usp=sharing

Alterações nas funções do processador do ICMC.
Grupo: Iara Duarte Mainates, Geraldo Murilo Carrijo Viana Alves da Silva e Lucas Caetano Procopio

Trabalho 1 de Org.Comp - Implementação no Processador ICMC

Função NOR: Relatório

Como surgiu a ideia
O grupo sugeriu a criação da função NOR pois esta é uma função lógica que pode
ser muito utilizada no campo da programação. Por isso, tivemos a ideia de
implementar essa função seguindo o roteiro de implementação de suas
companheiras - as outras funções lógicas.

O que a função faz
A função realiza a operação lógica NOR - ou seja a negação da operação OR.

Qual o seu caminho
A função NOR segue o mesmo caminho no processador que as outras operações
lógicas, ou seja: Considerando que iremos usar 3 registradores, pois um registrador
receberá o resultado da operação NOR entre dois registradores, temos:
    ● RX, RY, RZ
    ● RX recebe o resultado de RY NOR RZ
Inicialmente, selecionamos 2 registradores onde teremos os valores das variáveis
da operação (RY e RZ). Fazemos isso selecionando os valores através dos
multiplexadores 3 e 4 (M3 e M4).
Depois disso, lemos a operação através da MC e ao entender que é uma operação
NOR a ULA fará a operação, selecionamos a entrada do multiplexador 2 (M2) para
que o resultado da ULA possa passa. Também selecionamos o multiplexador 6 (M6)
que controla a passagem para o flag-register (FR) para casos de numeros negativos
ou outras situações.
Após isso, o resultado que foi calculado na ULA chegará até o registrado de
resultado (RX) e a função finaliza sua execução.

Mudanças no Código
As mudanças feitas para que o código do processador conseguisse entender a
função NOR foram divididas em duas grandes áreas: alterações no montador e
alterações no simulador.
    ● Alterações no Montador:
        ○ Acrescentamos a definição (defs.h) do código NOR na linha 105,
        como: #define NOR_CODE 98
        ○ Acrescentamos a definição (defs.h) da função NOR na linha 138,
        como: #define LNOR “010111”
        ○ Acrescentamos definição (defs.h) da string da chamada a função NOR
        na linha 219, como: #define NOR_STR “NOR”
        ○ Acrescentamos à função de detectar labels (montador.c) nossa
        função, na linha 208, que possui 3 argumentos e 1 linha.
        ○ Acrescentamos à função de montar instruções (montador.c) nossa
        função, a partir da linha 901, que pega todos os argumentos e monta
        para a instrução
        ○ Acrescentamos à função de buscar instruções (montador.c) nossa
        função, na linha 2304, que compara a instrução digitada e encaminha
        para a função correta
    ● Alterações no Simulador:
        ○ Acrescentamos a definição (Mneumonicos.h) da função lógica NOR,
        na linha 25, como: #define LNOR 23
        ○ Acrescentamos nas funções de visualização (View.cpp) a visualização
        da nossa função, na linha 240
        ○ Acrescentamos a nossa função, de fato, no arquivo Model.cpp, na
        linha 565, determinando todas as suas operações e tratamentos
